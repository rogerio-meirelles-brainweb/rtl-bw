import './index.scss';

import React, { useState } from 'react';

import { useIntl } from 'react-intl';

const Layout = () => {
  const intl = useIntl();
  const [flag, toggleFlag] = useState(false)

  return (
    <div className="layout">
      <a
        style={{ margin: 16 }}
        href={flag ? "https://www.bing.com/" : "https://www.google.com/"}
        target="_blank"
        rel="noopener noreferrer"
        data-testid="link-redirect"
      >
        Link
      </a>

      {intl.formatMessage({ id: flag ? 'go-to-bing' : 'go-to-google' })}

      <button
        style={{ margin: 16 }}
        onClick={() => toggleFlag(!flag)}
        data-testid="button-change-redirect"
      >
        {intl.formatMessage({ id: 'change-redirect' })}
      </button>
    </div>
  );
}

export default Layout;