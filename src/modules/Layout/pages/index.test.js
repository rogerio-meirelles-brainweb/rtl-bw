import { render, screen } from '@testing-library/react';

import { IntlProvider } from 'react-intl';
import Layout from '.';
import React from 'react';
import { messages } from 'modules/Intl';
import userEvent from '@testing-library/user-event'

const RTLWrapper = component => (
  <IntlProvider locale={"en"} messages={messages["en"]}>
    {React.cloneElement(component)}
  </IntlProvider>
);

describe('Layout', () => {
  it('Check initial render', () => {
    render(RTLWrapper(<Layout />));

    const linkElement = screen.getByTestId('link-redirect');
    expect(linkElement.href).toEqual('https://www.google.com/');

    const textElement = screen.getByText('Você será redirecionado para o google');
    expect(textElement).toBeInTheDocument();

    const buttonElement = screen.getByTestId('button-change-redirect');
    expect(buttonElement).toBeInTheDocument();
  });

  it('Changes redirect', () => {
    render(RTLWrapper(<Layout />));

    const buttonElement = screen.getByTestId('button-change-redirect');
    userEvent.click(buttonElement);

    const linkElement = screen.getByTestId('link-redirect');
    expect(linkElement.href).toEqual('https://www.bing.com/');

    const bingTextElement = screen.getByText('Agora será redirecionado para o bing');
    expect(bingTextElement).toBeInTheDocument();
  });
});
