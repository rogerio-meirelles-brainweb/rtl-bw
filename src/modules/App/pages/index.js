import { IntlProvider } from 'react-intl';
import Layout from 'modules/Layout';
import React from 'react';
import { messages } from 'modules/Intl';

const App = () => {
  const locale = "en";

  return (
    <div className="app">
      <IntlProvider locale={locale} messages={messages[locale]}>
        <Layout />
      </IntlProvider>
    </div>
  );
}

export default App;